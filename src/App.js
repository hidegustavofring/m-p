import React, { useState } from 'react';
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import { Provider } from "react-redux";
import store from "./redux/store";
import Navbar from "./Components/navBar";
import Cart from "./Components/cart";
import ProductList from "./Components/productList";
import HeroSection from "./Components/Hero"
import StoreDescriptionSection from "./Components/StoreDescriptionSection"
import Footer from "./Components/Footer"
import TestimonialsSection from "./Components/TestimonialsSection"
import 'bootstrap/dist/css/bootstrap.min.css';
import Login from "./Components/Login";


const App = () => {
  const [user, setUser] = useState(null);
  return (
    <Provider store={store}>
      <Router>
        <div>
          <Navbar />
          <Switch>
            <Route
              path="/ProductList"
              exact
              render={() => user ? <ProductList /> : <Login onLogin={(userData) => {setUser(userData);}}/>}
            />
            <Route path="/cart" component={Cart} />
            <Route path="/" exact>
              <HeroSection />
              <StoreDescriptionSection />
              <TestimonialsSection />
            </Route>
          </Switch>
          <Footer/>
        </div>
      </Router>
    </Provider>
  );
};

export default App;

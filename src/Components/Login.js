import React, { useState } from 'react';
import './Login.css';

const Login = ({ onLogin }) => {
  const [username, setUsername] = useState('');
  const [password, setPassword] = useState('');
  const [error, setError] = useState('');

  const handleLogin = () => {
    fetch('http://localhost:3001/users?username=' + username + '&password=' + password)
      .then(response => response.json())
      .then(data => {
        if (data.length > 0) {
          onLogin(data[0]);
          setError('');
        } else {
          setError('Invalid username or password');
        }
      })
      .catch(error => {
        console.error('Error fetching user data:', error);
        setError('An error occurred while logging in');
      });
  };

  return (
    <div className="login-container">
      <div className="login-card">
        <div className="card-body">
          <h2 className="card-title text-center mb-4">Login</h2>
          <form>
            <div className="form-group">
              <label>Username:</label>
              <input type="text" className="form-control" value={username} onChange={(e) => setUsername(e.target.value)} />
            </div>
            <div className="form-group">
              <label>Password:</label>
              <input type="password" className="form-control" value={password} onChange={(e) => setPassword(e.target.value)} />
            </div>
            <button type="button" className="btn btn-primary btn-block" onClick={handleLogin}>Login</button>
          </form>
          {error && <p className="text-danger mt-3">{error}</p>}
        </div>
      </div>
    </div>
  );
};

export default Login;

import React from "react";
import { useSelector, useDispatch } from "react-redux";
import { addQuantity, removeFromCart, removeQuantity } from "../redux/actions";
import "./Cart.css";

const Cart = () => {
  const cartItems = useSelector((state) => state.cartItems);
  const dispatch = useDispatch();

  const handleRemoveFromCart = (productId) => {
    dispatch(removeFromCart(productId));
  };

  const handleAddQuantity = (product) => {
    dispatch(addQuantity(product));
  };

  const handleRemoveQuantity = (product) => {
    dispatch(removeQuantity(product));
  };

  return (
    <div>
      {cartItems.length === 0 ? (
        <div className="text-center">
          <img src="/Images/9960436.jpg" alt="dasd" style={{ height: "80vh" }}/>
        </div>
      ) : (
        <div className="cart">
          <h2 style={{ textAlign: "center" }}>Your Products</h2>
          <ul>
            {cartItems.map((item) => (
              <li key={item.id} className="cart-item">
                <img src={`/Images/${item.image}`} alt={item.name} />
                <div className="cart-item-details">
                  <div className="cart-item-name">{item.name}</div>
                  <div className="cart-item-price">${item.price}</div>
                  <div className="cart-item-quantity">
                    <button onClick={() => handleRemoveQuantity(item)}>-</button>
                    <span>{item.quantity}</span>
                    <button onClick={() => handleAddQuantity(item)}>+</button>
                  </div>
                  <button
                    className="remove-button"
                    onClick={() => handleRemoveFromCart(item.id)}
                  >
                    Remove
                  </button>
                </div>
              </li>
            ))}
          </ul>
        </div>
      )}
    </div>
  );
};

export default Cart;

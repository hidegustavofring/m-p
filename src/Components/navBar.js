import React from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faShoppingCart } from "@fortawesome/free-solid-svg-icons";
import { useSelector } from "react-redux";
import { Link } from "react-router-dom";

const Navbar = () => {
  const cartItems = useSelector((state) => state.cartItems);
  const cartItemCount = cartItems.reduce(
    (total, item) => total + item.quantity,
    0
  );

  const isCartEmpty = cartItemCount === 0;

  return (
    <nav className="navbar navbar-expand-lg navbar-dark bg-dark">
      <div className="container">
        <Link to="/" className="navbar-brand">
          moroccanP
        </Link>

        <div className="collapse navbar-collapse">
          <ul className="navbar-nav ms-auto">
            {!isCartEmpty ? (
              <li className="nav-item">
                <Link to="/cart" className="nav-link">
                  <FontAwesomeIcon icon={faShoppingCart} />
                  <span className="ms-1 cart-count">{cartItemCount}</span>
                </Link>
              </li>
            ) : (
              <li className="nav-item disabled">
                <span className="nav-link disabled" tabIndex="-1">
                  <FontAwesomeIcon icon={faShoppingCart} />
                  <span className="ms-1 cart-count">{cartItemCount}</span>
                </span>
              </li>
            )}
          </ul>
        </div>
      </div>
    </nav>
  );
};

export default Navbar;

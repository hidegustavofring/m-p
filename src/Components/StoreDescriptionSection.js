import React from 'react';
import './StoreDescription.css';

const StoreDescriptionSection = () => {
  return (
    <div className="store-description-section">
      <div className="store-description-content">
        <h2>About Our Store</h2>
        <p>Welcome to our Moroccan traditional product store! We offer a wide range of authentic Moroccan products, including spices, textiles, ceramics, and more. Our products are sourced directly from local artisans and craftsmen, ensuring the highest quality and authenticity.</p>
      </div>
    </div>
  );
}

export default StoreDescriptionSection;

import React from 'react';
import { Link } from 'react-router-dom';
import './Hero.css'; 

const HeroSection = () => {
  return (
    <div className="hero-section">
      <div className="hero-content">
        <h1>Welcome to MoroccanP</h1>
        <p>Your one-stop destination for authentic Moroccan products</p>
        <Link to="/ProductList" className="btn btn-primary">see our products</Link>
      </div>
    </div>
  );
}

export default HeroSection;

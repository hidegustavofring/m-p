import React from 'react';

const TestimonialsSection = () => {
  return (
    <div className="container py-5" style={{ minHeight: '60vh' }}>
      <div className="row">
        <div className="col-md-12">
          <h2 className="mb-4 text-center">Testimonials</h2>
          <div className="row">
            <div className="col-md-4 mb-4">
              <div className="card h-100">
                <div className="card-body">
                  <p className="card-text">"The rug is really wonderful exactly as it's in the picture. It arrived clean and ready to use. Mohamed was very responsive and he helped me to choose the right products. He answered all my questions about Berber carpets. The price is very fair for a high quality handmade rugs. I highly recommend this company."</p>
                  <p className="card-text"><strong>Melissa</strong><br />Arizona</p>
                </div>
              </div>
            </div>
            <div className="col-md-4 mb-4">
              <div className="card h-100">
                <div className="card-body">
                  <p className="card-text">"I bought Moroccan handmade products from this shop because a friend of mine recommended them. I'm very satisfied after the first experience. The package arrived on time and it was exactly what I ordered. The Argan oil feels good. I recommend it for any sensitive skin. Thank you for the team of Morocco Products."</p>
                  <p className="card-text"><strong>Tiffany</strong><br />Ottawa</p>
                </div>
              </div>
            </div>
            <div className="col-md-4 mb-4">
              <div className="card h-100">
                <div className="card-body">
                  <p className="card-text">"I met Mohamed for the first time in 2017 while visiting Morocco for a 14 days trip. He is a very friendly and helpful person. I ordered some Moroccan bio cosmetics (Shampoo, Soap, Argan Oil). I got them on time and they are good products for the skin. As it's hard to trust online shopping, I recommend you to contact Mohamed. He is reliable and you can trust."</p>
                  <p className="card-text"><strong>Maria</strong><br />Handmade Morocco</p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default TestimonialsSection;
